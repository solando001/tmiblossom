<?php

namespace App\Http\Controllers;

use App\Models\Enrollment;
use App\Models\Event;
use App\Models\News;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function index()
    {
        $news = \App\Models\News::orderBy('created_at', 'desc')->take(3)->get();
        $events = Event::orderBy('created_at', 'desc')->take(3)->get();
        return view('index', ['news' => $news, 'events' => $events]);
    }

    public function news()
    {
        $news = \App\Models\News::orderBy('created_at', 'desc')->paginate(10);
        return view('news', ['news' => $news]);
    }

    public function news_details($slug)
    {
        $news_details = News::where('slug', $slug)->first();
        return view('news_details', ['details' => $news_details]);
    }
    public function enroll()
    {
        return view('enrol');
    }
    public function post_enroll(Request $request)
    {
        $this->validate($request, [
            'parent_phone' => ['required','numeric','digits:11'],
            'child_age' => ['required','numeric'],
        ]);

        $save = Enrollment::create($request->all());
        $save->save();

        return back()->with(['success' => 'Enrollment Successful']);
    }
    public function teaching_staff()
    {
        return view('teacher');
    }

    public function non_teaching_staff()
    {
        return view('non_teacher');
    }

    public function event()
    {
        $event = Event::orderBy('created_at', 'desc')->get();
        return view('event', ['events' => $event]);
    }

    public function event_details($slug)
    {
        $details = Event::where('slug', $slug)->first();
        return view('event_details', ['details' => $details]);
    }

    public function gallery()
    {
        return view('gallery');
    }

    public function contact()
    {
        return view('contact');
    }

    public function about()
    {
        return view('about');
    }

    public function curriculum()
    {
        return view('curriculum');
    }

    public function proprietress()
    {
        return view('proprietress');
    }
}
