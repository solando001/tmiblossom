<?php

namespace App\Http\Controllers;

use App\Models\Enrollment;
use App\Models\Event;
use App\Models\News;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use League\CommonMark\Node\NodeWalker;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(100);
        $events = Event::orderBy('created_at', 'desc')->paginate(100);
        $staffs = Enrollment::orderBy('created_at', 'desc')->paginate(100);
        //$staffs = Staff::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.home', ['staffs' => $staffs, 'events' => $events, 'news' => $news]);
    }

    public function add_news()
    {
        return view('admin.news');
    }

    public function add_event()
    {
        return view('admin.event');
    }

    public function add_staff()
    {
        return view('admin.staff');
    }

    public function post_news(Request $request)
    {
        //dd($request);
        if($request->hasFile('image')) {
            $extension = time().'.'.$request->image->extension();
            $url = $request->image->move('news_product', $extension);

            $news=  News::create([
                'user_id' => Auth::user()->id,
                'title' => $request->title,
                'slug'=> '',
                'message' => $request->message,
                'image' => $url,
                'count' => 0,
            ]);

            $chars = "0123456789";
            $code = "";
            for ($i = 0; $i < 3; $i++) {
                $code .= $chars[mt_rand(0, strlen($chars) - 1)];
            }
            $str = Str::slug($request->title, '-');
            $slug = $str . -$code;
            $news->update(['slug' => $slug]);
            return back()->with(['success' => 'News Created Successfully']);
        }
    }

    public function post_event(Request $request)
    {
        $dt = date('Y-M-d H:i:s', strtotime($request->date));

        if($request->hasFile('image')) {
            $extension = time().'.'.$request->image->extension();
            $url = $request->image->move('event_img', $extension);

            $event = Event::create([
                'user_id' => Auth::user()->id,
                'title' => $request->title,
                'slug' => '',
                'message' => $request->message,
                'date' => $dt,
                'image' => $url,
            ]);

            $chars = "0123456789";
            $code = "";
            for ($i = 0; $i < 2; $i++) {
                $code .= $chars[mt_rand(0, strlen($chars) - 1)];
            }
            $str = Str::slug($request->title, '-');
            $slug = $str . -$code;
            $event->update(['slug' => $slug]);
            return back()->with(['success' => 'Event Created Successfully']);
        }
    }

    public function post_staff(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|numeric|digits:11|unique:staff',
            'email' => 'required|string|unique:staff|email',
        ]);

        //dd($request);

        if($request->hasFile('image')) {
            $extension = time().'.'.$request->image->extension();
            $url = $request->image->move('staff_img', $extension);

            Staff::create([
                'user_id' => Auth::user()->id,
                'title' => $request->title,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'image' => $url,
            ]);
            return back()->with(['success' => 'Staff Created Successfully']);
        }

    }
}
