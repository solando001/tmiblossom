<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrative',
            'email' => 'solotobby@gmail.com',
            'password' => Hash::make('solomon001'),
        ]);

        User::create([
            'name' => 'Farohunbi Samuel Tobi',
            'email' => 'samuel.st@gmail.com',
            'password' => Hash::make('testimonies001'),
        ]);

        User::create([
            'name' => 'Covenant Child School Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin001'),
        ]);
    }
}
