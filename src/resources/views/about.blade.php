@extends('layouts.master')
@section('title', 'About Us')
@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>About</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>About</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Who We Are Area -->
    <section class="who-we-are ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="who-we-are-content">
{{--                        <span>About Us</span>--}}
                        <h4>Our Vision</h4>
                        <p>
                            To raise solution providers.
                        </p>
                        <br>

                        <h4>Our mission</h4>
                        <p>
                            Blossom Academy is a school where the curriculum is progressively developed to holistically cater for the child's overall nourishment in life.

                        <ul class="who-we-are-list">
                            <li>
                                <span>1</span>
                                Offering activity based learning.
                            </li>
                            <li>
                                <span>2</span>
                                Raising effective and purpose driven leaders.
                            </li>
                            <li>
                                <span>3</span>
                                Building good moral, academic and spiritual values.
                            </li>
{{--                            <li>--}}
{{--                                <span>4</span>--}}
{{--                                Play to Learn--}}
{{--                            </li>--}}
                        </ul>

                        </p>
                        <br>
                        <h4>Our Core Values</h4>
                        <p>
                            Spirituality, Character, Possibility, Creativity, Excellence, Greatness, Role Model.
                        </p>
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                        <ul class="who-we-are-list">--}}
{{--                            <li>--}}
{{--                                <span>1</span>--}}
{{--                                Homelike Environment--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span>2</span>--}}
{{--                                Quality Educators--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span>3</span>--}}
{{--                                Safety and Security--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span>4</span>--}}
{{--                                Play to Learn--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <div class="who-we-are-btn">--}}
{{--                            <a href="#" class="default-btn">Read More</a>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="who-we-are-image-wrap">
                        <img src="{{asset('/assets/ga5.jpg')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Who We Are Area -->

    <!-- Start Fun Facts Area -->
    <section class="fun-facts-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="450">00</span>
                        </h3>
                        <p>Students</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-1">
                        <h3>
                            <span class="odometer" data-count="24">00</span>
                        </h3>
                        <p>Teachers</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-2">
                        <h3>
                            <span class="odometer" data-count="26">00</span>
                        </h3>
                        <p>Classrooms</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-3">
                        <h3>
                            <span class="odometer" data-count="2">00</span>
                        </h3>
                        <p>Buses</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Fun Facts Area -->



@endsection
