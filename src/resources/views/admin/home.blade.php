@extends('layouts.master')
@section('title', 'Admin Home')
@section('content')
    <!-- Start Fun Facts Area -->
    <section class="fun-facts-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="1200">00</span>
                        </h3>
                        <p>Students</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-1">
                        <h3>
                            <span class="odometer" data-count="305">00</span>
                        </h3>
                        <p>Teaching Staff</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-2">
                        <h3>
                            <span class="odometer" data-count="48">00</span>
                        </h3>
                        <p>Non-Teaching Staff</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact bg-3">
                        <h3>
                            <span class="odometer" data-count="50">00</span>
                        </h3>
                        <p>News</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Fun Facts Area -->

    <!-- Start Pricing Area -->
    <div class="pricing-area ptb-100">
        <div class="container">
            <div class="section-title">
{{--                <span>Pricing</span>--}}
                <h2>News List</h2>
            </div>

            <div class="pricing-table table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Title
                        </th>
                        <th>
                            Visits
                        </th>
                        <th>
                            When Created
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($news as $new)
                    <tr>
                        <th><a href=""> {{$new->title}}</a></th>
                        <td>{{$new->count}}</td>
                        <td>{{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="table-title">
                    {{$news->links()}}
                </div>
            </div>
        </div>
    </div>
    <!-- End Pricing Area -->

    <!-- Start Pricing Area -->
    <div class="pricing-area ptb-100">
        <div class="container">
            <div class="section-title">
                {{--                <span>Pricing</span>--}}
                <h2>Event List</h2>
            </div>

            <div class="pricing-table table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Title
                        </th>
                        <th>
                            Event Date
                        </th>
                        <th>
                            When Created
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($events as $event)
                    <tr>
                        <th>{{$event->title}}</th>
                        <td>{{$event->date}}</td>
                        <td>{{\Carbon\Carbon::parse($event->created_at)->diffForHumans()}}</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="table-title">
                    <p>{{$events->links()}}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Pricing Area -->


    <div class="pricing-area ptb-100">
        <div class="container">
            <div class="section-title">
                {{--                <span>Pricing</span>--}}
                <h2>Enrollment List</h2>
            </div>

            <div class="pricing-table table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Parent Name
                        </th>
                        <th>
                            Parent Phone
                        </th>
                        <th>
                            Parent Email
                        </th>
                        <th>
                             Child Name
                        </th>
                        <th>
                            Gender/Age
                        </th>
                        <th>
                                Entry Class
                        </th>
                        <th>
                            Address
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($staffs as $staff)
                        <tr>
                            <th>{{$staff->parent_name}}</th>
                            <td>{{$staff->parent_phone}}</td>
                            <td>{{$staff->parent_email}}</td>
                            <td>{{$staff->child_name}}</td>
                            <td>{{$staff->child_gender}}/{{$staff->child_age}}</td>
                            <td>{{$staff->child_class}}</td>
                            <td>{{$staff->address}}</td>
                            <td>{{\Carbon\Carbon::parse($staff->created_at)->diffForHumans()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="table-title">
                    <p>{{$staffs->links()}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
