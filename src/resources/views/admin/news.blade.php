@extends('layouts.master')
@section('title', 'Add News')
@section('content')

    <!-- Start Login Area -->
    <section class="apply-area ptb-100">
        <div class="container">
            <div class="apply-form">
                <h2>Add News</h2>

                @if(session('success'))
                    <div class="alert alert-success alert-block" id="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ session('success') }}
                    </div>
                @endif


                <form  action="{{url('post/news')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input id="email" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Title" required>

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Content</label>
                        <textarea name="message" id="message" cols="30" rows="15" class="form-control" placeholder="Write your content..." required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Image</label><br>
                        <input type="file" name="image" required>
                    </div>




                    <button type="submit" class="default-btn">
                        Submit Now
                    </button>

                </form>

                {{--                <div class="important-text">--}}
                {{--                    <p>Don't have an account? <a href="register.html">Register now!</a></p>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Login Area -->
    @endsection
