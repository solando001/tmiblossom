@extends('layouts.master')
@section('title', 'Admin Login')
@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Login</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Login</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Login Area -->
    <section class="login-area ptb-100">
        <div class="container">
            <div class="login-form">
                <h2>Login</h2>

                <form  action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Email or phone</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-check">
                                {{--                                <input type="checkbox" class="form-check-input" id="checkme">--}}
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">Remember me</label>
                            </div>
                        </div>

                        {{--                        <div class="col-lg-6 col-md-6 col-sm-6 lost-your-password">--}}
                        {{--                            <a href="#" class="lost-your-password">Forgot your password?</a>--}}
                        {{--                        </div>--}}
                    </div>

                    <button type="submit">Login</button>
                </form>

                {{--                <div class="important-text">--}}
                {{--                    <p>Don't have an account? <a href="register.html">Register now!</a></p>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Login Area -->
@endsection
