@extends('layouts.master')
@section('title', 'Our Curriculum')
@section('content')
    <div class="page-banner-area item-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Our Curriculum</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Curriculum</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Privacy policy Area -->
    <section class="privacy-policy-area ptb-100">
        <div class="container">
            <div class="privacy-policy-accordion">
                <ul class="accordion">
                    <li class="accordion-item">
                        <a class="accordion-title active" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Our Curriculum
                        </a>

                        <p class="accordion-content show">
                            TMI Blossom Academy uses the recommended curriculum of the Federal Government of Nigeria for primary schools.
                            In addition to this, the school teaches spiritual wisdom, music/instrumentation and also integrates the Christian
                            worldview throughout each academic subject as well as in all other areas including sports, cultural activities, camps,
                            excursions etc.

                            <br>
                            Our goal is to educate the ‘whole child’ – spiritual, mental, social and physical. Our aim is to teach pupils using a
                            range of methods to accommodate the verity of learning styles that different individuals have, incorporating a range of
                            specifically taught thinking skills and in a variety of learning contexts, such as outdoors and excursions.

                            <br>
                            Our teachers are trained specifically to present Christian perspectives in their area of expertise. They strive to demonstrate
                            the love of God through their daily lives beyond the classrooms. This consistency between Christian teaching and Christian
                            living is one of the key strengths of our community. Staff members and pupils meet each morning for devotions and prayer.
                            These are focused on the activities of the day and the needs of students and other community members. Our staff genuinely care
                            about the welfare of pupils and their life journeys. Pupils also begin each day with a class devotion led by a teacher who has
                            responsibility for their pastoral care. This structure encourages the development of relationships between pupils and teachers that
                            are based on trust and mutual respect. The following subjects are taught by season teachers at TMI Blossom Academy. Subjects offered
                            in the school are grouped under various departments which include:

                            <br><br>
                            <b>Mathematics Department</b>
                            <br>
                            1. General Mathematics<br>
                            i. Arithmetic<br>
                            ii. Geometric<br>
                            iii. Algebra<br>
                            2. Further Mathematics<br><br>

                            <b>Language Department</b>
                            <br>
                            1. English Language<br>
                            i. Essay<br>
                            ii. Grammar<br>
                            ii. Oral<br>
                            2. French<br>
                            3. Literature in English<br>

                            <br>
                            <b>Science Department</b><br>
                            1 Physics<br>
                            2. Chemistry<br>
                            3. Biology<br>
                            4. Physical and Health Education<br>
                            5. Basic Science<br>
                            6 Agricultural Science<br>

                            <br>
                            <b>Social Science Department</b><br>
                            1. Social studies<br>
                            2. Government<br>
                            3. Civic Education<br>
                            4. Economics<br>
                            5. Commerce<br>
                            6. Financial Accounting<br>

                            <br>
                            <b>Arts Department</b><br>
                            1. Religious Education<br>
                            2. Creative/Visual Arts<br>
                            3. Music<br>
                            <br>
                            <b>Vocational / Entrepreneurial Department</b><br>
                            1. Basic Technology<br>
                            2. Computer Studies<br>
                            3. Food and Nutrition<br>
                            4. Home Economics<br>
                            5. Clothing and Textile<br>
                            6. Data processing<br>
                            7. Dyeing and Bleaching<br><br>


                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Privacy policy Area -->


@endsection
