@extends('layouts.master')
@section('title', 'Enroll My Child')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Child Enrollment</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Enrollment</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Apply Area -->
    <section class="apply-area ptb-100">
        <div class="container">
            <div class="apply-form">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                <form action="{{url('enroll')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="content">
                                <h3>Parent Details</h3>
                            </div>

                            <div class="form-group">
                                <input type="text" name="parent_name" class="form-control" placeholder="Your Name" value="{{old('parent_name')}}" required>
                            </div>

                            <div class="form-group">
                                <input type="email" name="parent_email" class="form-control" placeholder="Email Address" value="{{old('parent_email')}}" required>
                            </div>

                            <div class="form-group">
                                <input type="text" name="parent_phone" class="form-control" placeholder="Phone Number" value="{{old('parent_phone')}}" required>
                            </div>
                            <div class="form-group">
                                <select name="relationship" class="form-control" required>
                                    <option value="">Select Relationship</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Father">Father</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="content">
                                <h3>Child Details</h3>
                            </div>

                            <div class="form-group">
                                <input type="text" name="child_name" class="form-control" placeholder="Name" value="{{old('child_name')}}" required>
                            </div>

                            <div class="form-group">
                                <select name="child_gender" class="form-control" required>
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" name="child_age" class="form-control" placeholder="Age" value="{{old('child_age')}}" required>
                            </div>

                            <div class="form-group">
                                <select name="child_class" class="form-control" required>
                                    <option value="">Select Class</option>
                                    <option value="RECEPTION 1">RECEPTION 1</option>
                                    <option value="RECEPTION 2">RECEPTION 2</option>
                                    <option value="KG 1">Kindergarten 1</option>
                                    <option value="KG 2">Kindergarten 2</option>
                                    <option value="PRI 1">Primary 1</option>
                                    <option value="PRI 2">Primary 2</option>
                                    <option value="PRI 3">Primary 3</option>
                                    <option value="PRI 4">Primary 4</option>
                                    <option value="PRI 5">Primary 5</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="address" placeholder="Address"  required>{{old('address')}}</textarea>
                    </div>
                    <input type="hidden" name="is_admitted" value="0">

                    <button type="submit" class="default-btn">
                        Submit Now
                    </button>
                </form>
            </div>
        </div>
    </section>
    <!-- End Apply Area -->


    @endsection
