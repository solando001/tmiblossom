@extends('layouts.master')
@section('title', 'Gallery')
@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Gallery</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Gallery Area -->
    <div class="gallery-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/nw4.jpg')}}" alt="image">

                        <a href="{{asset('assets/nw4.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/ga2.jpg')}}" alt="image">

                        <a href="{{asset('assets/ga2.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/ga3.jpg')}}" alt="image">

                        <a href="{{asset('assets/ga3.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/ga4.jpg')}}" alt="image">

                        <a href="{{asset('assets/ga4.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/ga5.jpg')}}" alt="image">

                        <a href="{{asset('assets/ga5.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/ga6.jpg')}}" alt="image">

                        <a href="{{asset('assets/ga6.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/nw1.jpg')}}" alt="image">

                        <a href="{{asset('assets/nw1.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/nw2.jpg')}}" alt="image">

                        <a href="{{asset('assets/nw2.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <img src="{{asset('assets/nw3.jpg')}}" alt="image">

                        <a href="{{asset('assets/nw3.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">
                            <i class='bx bx-search-alt'></i>
                        </a>
                    </div>
                </div>

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw4.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw4.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw5.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw5.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw6.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw6.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw7.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw7.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw8.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw8.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw9.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw9.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw10.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw10.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw11.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw11.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw12.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw12.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw13.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw13.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw14.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw14.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw15.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw15.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw16.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw16.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw17.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw17.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw18.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw18.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw19.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw19.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}


{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw20.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw20.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-gallery-box">--}}
{{--                        <img src="{{asset('assets/nw21.jpg')}}" alt="image">--}}

{{--                        <a href="{{asset('assets/nw21.jpg')}}" class="gallery-btn" data-imagelightbox="popup-btn">--}}
{{--                            <i class='bx bx-search-alt'></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
    </div>
    <!-- End Gallery Area -->


@endsection
