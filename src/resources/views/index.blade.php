@extends('layouts.master')
@section('title', 'TMIBLOSSOM ACADEMY')
@section('content')
    <!-- Start Main Banner Area -->
    <div class="main-banner">
        <div class="main-banner-item">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container-fluid">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6">
                                            <div class="main-banner-content">
                                                <span style="color: #000080; font-size: xx-large">we are committed to</span>
                                                <h1>Raising Solution Providers</h1>
{{--                                                <p>At TMI Blossom Academy, we seek to approach academics in a unique way where our students will be sort after in their chosen field.</p>--}}
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="main-banner-image">
                                                <img src="{{asset('assets/home.jpg')}}" alt="image">
                                            </div>
                                        </div>
                                    </div>
{{--                                    <img src="..." class="d-block w-100" alt="...">--}}
                                </div>
                                <div class="carousel-item">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6">
                                            <div class="main-banner-content">
                                                <span style="color: #000080; font-size: xx-large">we are committed to</span>
                                                <h1>Raising Solution Providers</h1>
{{--                                                <p>At TMI Blossom Academy, we seek to approach academics in a unique way where our students will be sort after in their chosen field.</p>--}}
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="main-banner-image">
                                                <img src="{{asset('assets/ga2.jpg')}}" alt="image">
                                            </div>
                                        </div>
                                    </div>
{{--                                    <img src="..." class="d-block w-100" alt="...">--}}
                                </div>
                                <div class="carousel-item">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6">
                                            <div class="main-banner-content">
                                                <span style="color: #000080; font-size: xx-large">we are committed to</span>
                                                <h1>Raising Solution Providers</h1>
{{--                                                <p>At TMI Blossom Academy, we seek to approach academics in a unique way where our students will be sort after in their chosen field.</p>--}}
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="main-banner-image">
                                                <img src="{{asset('assets/nw4.jpg')}}" alt="image">
                                            </div>
                                        </div>
                                    </div>
{{--                                    <img src="..." class="d-block w-100" alt="...">--}}
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner-shape">
            <div class="banner-bg-shape">
                <img src="assets/img/main-banner/banner-bg-shape-1.png" alt="image">
            </div>

            {{--                <div class="shape-1">--}}
            {{--                    <img src="assets/img/main-banner/banner-shape-1.png" alt="image">--}}
            {{--                </div>--}}

            {{--                <div class="shape-2">--}}
            {{--                    <img src="assets/img/main-banner/banner-shape-2.png" alt="image">--}}
            {{--                </div>--}}

            {{--                <div class="shape-3">--}}
            {{--                    <img src="assets/img/main-banner/banner-shape-3.png" alt="image">--}}
            {{--                </div>--}}

            {{--                <div class="shape-4">--}}
            {{--                    <img src="assets/img/main-banner/banner-shape-4.png" alt="image">--}}
            {{--                </div>--}}
        </div>
    </div>
    <!-- End Main Banner Area -->

    <!-- Start Who We Are Area -->
    <section class="who-we-are ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="who-we-are-image">
                        <img src="{{asset('assets/img1.jpg')}}" alt="image">
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="who-we-are-content">
{{--                        <span style="color: #000080">Who We Are</span>--}}
                        <h3>Who We Are</h3>
                        <p>
                            We are an institution of learning that prefers an holistic approach to learning rather than just the peripheral.
                        </p>
                        <p>
                            We strive to give our students the best. With sound and qualified teachers at our disposal, excellence is a reality.
                        </p>
                        <ul class="who-we-are-list">
                            <li>
                                <span>1</span>
                                Homelike Environment
                            </li>
                            <li>
                                <span>2</span>
                                Quality Educators
                            </li>
                            <li>
                                <span>3</span>
                                Safety and Security
                            </li>
                            <li>
                                <span>4</span>
                                Play to Learn
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

{{--        <div class="who-we-are-shape">--}}
{{--            <img src="assets/img/who-we-are/who-we-are-shape.png" alt="image">--}}
{{--        </div>--}}
    </section>
    <!-- End Who We Are Area -->

    <!-- Start Class Area -->
{{--    <section class="class-area bg-fdf6ed pt-100 pb-70">--}}
{{--        <div class="container">--}}
{{--            <div class="section-title">--}}
{{--                <span style="color: #000080">Classes</span>--}}
{{--                <h2>Popular Classes</h2>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="assets/img/class/class-1.jpg" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Age:</span>--}}
{{--                                --}}{{--                                        3-5 Year--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Time:</span>--}}
{{--                                --}}{{--                                        8-10 AM--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            --}}{{--                                <div class="class-btn">--}}
{{--                            --}}{{--                                    <a href="#" class="default-btn">Join Class</a>--}}
{{--                            --}}{{--                                </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="assets/img/class/class-1.jpg" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Age:</span>--}}
{{--                                --}}{{--                                        3-5 Year--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Time:</span>--}}
{{--                                --}}{{--                                        8-10 AM--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            --}}{{--                                <div class="class-btn">--}}
{{--                            --}}{{--                                    <a href="#" class="default-btn">Join Class</a>--}}
{{--                            --}}{{--                                </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-class">--}}
{{--                        <div class="class-image">--}}
{{--                            <a href="#">--}}
{{--                                <img src="assets/img/class/class-1.jpg" alt="image">--}}
{{--                            </a>--}}
{{--                        </div>--}}

{{--                        <div class="class-content">--}}
{{--                            <h3>--}}
{{--                                <a href="#">Color Matching</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

{{--                            <ul class="class-list">--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Age:</span>--}}
{{--                                --}}{{--                                        3-5 Year--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                --}}{{--                                    <li>--}}
{{--                                --}}{{--                                        <span>Time:</span>--}}
{{--                                --}}{{--                                        8-10 AM--}}
{{--                                --}}{{--                                    </li>--}}
{{--                                <li>--}}
{{--                                    <span>Seat:</span>--}}
{{--                                    25--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                            --}}{{--                                <div class="class-btn">--}}
{{--                            --}}{{--                                    <a href="#" class="default-btn">Join Class</a>--}}
{{--                            --}}{{--                                </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="class-shape">--}}
{{--            --}}{{--                <div class="shape-1">--}}
{{--            --}}{{--                    <img src="assets/img/class/class-shape-1.png" alt="image">--}}
{{--            --}}{{--                </div>--}}
{{--            --}}{{--                <div class="shape-2">--}}
{{--            --}}{{--                    <img src="assets/img/class/class-shape-2.png" alt="image">--}}
{{--            --}}{{--                </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--    <!-- End Class Area -->--}}

    <!-- Start Value Area -->
    <section class="value-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="value-image">
                        <img src="{{asset('assets/img2.jpg')}}" alt="image">
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="value-item">
                        <div class="value-content">
                            <span>Our Core Values</span>
                            <h3>We are raising students of great minds and might!</h3>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span>01</span>
                            </div>
                            <h4>Active Learning</h4>
                            <p>We train and model well rounded students by providing quality education provided by seasoned and qualified teachers</p>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span class="bg-2">02</span>
                            </div>
                            <h4>Well Equipped Laboratories</h4>
                            <p>Our Computer, Science and Technology Laboratories are well equipped to aid excellence teaching and learning.</p>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span class="bg-3">03</span>
                            </div>
                            <h4>Serene Environment</h4>
                            <p>We have a safe and serene environment for effective teaching and learning</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="value-shape">
            {{--                <div class="shape-1">--}}
            {{--                    <img src="assets/img/value/value-shape-1.png" alt="image">--}}
            {{--                </div>--}}
            {{--                <div class="shape-2">--}}
            {{--                    <img src="assets/img/value/value-shape-2.png" alt="image">--}}
            {{--                </div>--}}
            {{--                <div class="shape-3">--}}
            {{--                    <img src="assets/img/value/value-shape-3.png" alt="image">--}}
            {{--                </div>--}}
        </div>
    </section>
    <!-- End Value Area -->

    <!-- Start Teacher Area -->
    <section class="teacher-area bg-ffffff pt-100 pb-70">
        <div class="container-fluid">
            <div class="section-title">
                <span style="color: #000080">Our Core Teacher</span>
                <h2>Meet Our Teachers</h2>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <div class="image">
                            <img src="{{asset('assets/teacher.jpg')}}" alt="image">
                        </div>

                        <div class="content">
                            <h3>George Okubama</h3>
                            <span>Head of Upper School, TMI BLOSSOM ACADEMY</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <div class="image">
                            <img src="{{asset('assets/img/dos.jpg')}}" alt="image">
                        </div>

                        <div class="content">
                            <h3>Mrs Kehinde Sarumi</h3>
                            <span>Director of Schools</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single-gallery-box">
                        <div class="images">
                            <img src="{{asset('CCProp.jpg')}}" alt="image">
                        </div>

                        <div class="content">
                            <h5>Pastor (Mrs) Oluseyi Aboyeji</h5>
                            <span>Proprietress</span>
                        </div>

                    </div>
                </div>



            </div>
        </div>
    </section>
    <!-- End Teacher Area -->

    <section class="value-area ptb-100">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6">
                    <div class="value-item">
                        <div class="value-content">
{{--                            <span>Our Core Values</span>--}}
                            <h3>Our Core Values</h3>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span>01</span>
                            </div>
                            <h4>Our Vision</h4>
                            <p>To raise solution providers</p>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span class="bg-2">02</span>
                            </div>
                            <h4>Our Mission</h4>
                            <p>TMI Blossom Academy is a faith-based school where the curriculum is
                                progressively developed to holistically cater for the child's overall nourishment in life.
                                <br>
                                1. Offering activity based learning.<br>
                                2. Raising effective and purpose driven leaders.<br>
                                3.  Building good moral, academic and spiritual values.
                            </p>
                        </div>

                        <div class="value-inner-content">
                            <div class="number">
                                <span class="bg-3">03</span>
                            </div>
                            <h4>Serene Environment</h4>
                            <p>We have a safe and serene environment for effective teaching and learning</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="value-image">
                        <img src="{{asset('assets/ga2.jpg')}}" alt="image">
                    </div>
                </div>

            </div>
        </div>

        <div class="value-shape">
            {{--                <div class="shape-1">--}}
            {{--                    <img src="assets/img/value/value-shape-1.png" alt="image">--}}
            {{--                </div>--}}
            {{--                <div class="shape-2">--}}
            {{--                    <img src="assets/img/value/value-shape-2.png" alt="image">--}}
            {{--                </div>--}}
            {{--                <div class="shape-3">--}}
            {{--                    <img src="assets/img/value/value-shape-3.png" alt="image">--}}
            {{--                </div>--}}
        </div>
    </section>

    <!-- Start Testimonials Area -->
{{--    <section class="testimonials-area pt-100 pb-100">--}}
{{--        <div class="container">--}}
{{--            <div class="section-title">--}}
{{--                <span style="color: #000080">Testimonials</span>--}}
{{--                <h2>What Parents Say About Us</h2>--}}
{{--            </div>--}}

{{--            <div class="testimonials-slides owl-carousel owl-theme">--}}
{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Glims Bond</h3>--}}
{{--                            <span>Music Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-1.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Sherlock Bin</h3>--}}
{{--                            <span>Art Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-2.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Priestly Herbart</h3>--}}
{{--                            <span>Math Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-3.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Glims Bond</h3>--}}
{{--                            <span>Music Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-1.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Sherlock Bin</h3>--}}
{{--                            <span>Head Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-2.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}

{{--                <div class="testimonials-item">--}}
{{--                    <div class="testimonials-item-box">--}}
{{--                        <div class="icon">--}}
{{--                            <i class='bx bxs-quote-left'></i>--}}
{{--                        </div>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>--}}
{{--                        <div class="info-box">--}}
{{--                            <h3>Priestly Herbart</h3>--}}
{{--                            <span>Math Teacher</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--                        <div class="testimonials-image">--}}
{{--                    --}}{{--                            <img src="assets/img/testimonials/testimonials-3.png" alt="image">--}}
{{--                    --}}{{--                        </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Testimonials Area -->

    <!-- Start Event Area -->
    <section class="event-area bg-ffffff pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span>Events</span>
                <h2>Upcoming Events</h2>
            </div>
            @foreach($events as $event)
            <div class="event-box-item">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <div class="event-image">
                            <a href="#"><img src="{{$event->image}}" alt="image"></a>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="event-content">
                            <h3>
                                <a href="{{url('event/details/'.$event->slug)}}">{{$event->title}}</a>
                            </h3>
                            <ul class="event-list">
                                <li>
                                    <i class='bx bx-time'></i>
                                    {{$event->date}}
                                </li>
                                <li>
                                    <i class='bx bxs-map'></i>
                                    No. 5b Unity Road Ilorin, Kwara State
                                </li>
                            </ul>
                        </div>
                    </div>

{{--                    <div class="col-md-3">--}}
{{--                        <div class="event-date">--}}
{{--                            <h4>12</h4>--}}
{{--                            <span>September</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
                @endforeach

        </div>
    </section>
    <!-- End Event Area -->

    <!-- Start Blog Area -->
    <section class="blog-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span style="color: #000080">News and Blog</span>
                <h2>Latest News</h2>
            </div>

            <div class="row">
                @foreach($news as $new)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <div class="blog-image">
                                <a href="{{url('news/details/'.$new->slug)}}">
                                    <img src="{{$new->image}}" alt="image">
                                </a>
                            </div>

                            <div class="blog-content">
                                <ul class="post-meta">
                                    {{--                                <li>--}}
                                    {{--                                    <span>By Admin:</span>--}}
                                    {{--                                    <a href="#">{{$new->user->name}}</a>--}}
                                    {{--                                </li>--}}
                                    <li>
                                        <span>Created:</span>
                                        {{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}
                                    </li>
                                </ul>
                                <h3>
                                    <a href="{{url('news/details/'.$new->slug)}}">{{$new->title}}</a>
                                </h3>
                                <p>
                                    {{--                                {{(substr($new->message, 0, 250)). '...'}}--}}
                                    {!! substr(strip_tags($new->message) , 0, 200). '...' !!}
                                </p>

                                {{--                            <div class="blog-btn">--}}
                                {{--                                <a href="#" class="default-btn">Read More</a>--}}
                                {{--                            </div>--}}
                            </div>
                        </div>
                    </div>
                @endforeach


                {{--                    <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">--}}
                {{--                        <div class="single-blog-item">--}}
                {{--                            <div class="blog-image">--}}
                {{--                                <a href="#">--}}
                {{--                                    <img src="assets/img/blog/blog-3.jpg" alt="image">--}}
                {{--                                </a>--}}
                {{--                            </div>--}}

                {{--                            <div class="blog-content">--}}
                {{--                                <ul class="post-meta">--}}
                {{--                                    <li>--}}
                {{--                                        <span>By Admin:</span>--}}
                {{--                                        <a href="#">Smith Broke</a>--}}
                {{--                                    </li>--}}
                {{--                                    <li>--}}
                {{--                                        <span>Date:</span>--}}
                {{--                                        27 Dec 2020--}}
                {{--                                    </li>--}}
                {{--                                </ul>--}}
                {{--                                <h3>--}}
                {{--                                    <a href="blog-details.html">Full-Day Session With Activities</a>--}}
                {{--                                </h3>--}}
                {{--                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}

                {{--                                <div class="blog-btn">--}}
                {{--                                    <a href="#" class="default-btn">Read More</a>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
            </div>
        </div>
    </section>
    <!-- End Blog Area -->

@endsection
