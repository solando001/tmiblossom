<!-- Start Footer Area -->
<section class="footer-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget">
{{--                    <div class="logo">--}}
{{--                        <h2>--}}
{{--                            <a href="{{url('/')}}">TMIBLOSSOM</a>--}}
{{--                            <img src="{{asset('assets/logo.png')}}" width="100px" height="50px">--}}
{{--                        </h2>--}}
{{--                    </div>--}}
                    <p>We are an institution of learning that prefers an holistic approach to learning rather than just the peripheral.</p>
                    <ul class="social">
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-facebook'></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-twitter'></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-pinterest-alt'></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-linkedin'></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Contact Us</h3>

                    <ul class="footer-contact-info">
                        <li>
                            <i class='bx bxs-phone'></i>
                            <span>Phone</span>
                            <a href="tel:08067540131">08067540131, 08075275107</a>
                        </li>
                        <li>
                            <i class='bx bx-envelope'></i>
                            <span>Email</span>
                            <a href="mailto:tmiblossomacademy@gmail.com">tmiblossomacademy@gmail.com</a>
                        </li>
                        <li>
                            <i class='bx bx-map'></i>
                            <span>Address</span>
                            No. 5b Unity Road Ilorin, Kwara State
                        </li>
                    </ul>
                </div>
            </div>

            {{--            <div class="col-lg-3 col-sm-6">--}}
            {{--                <div class="single-footer-widget pl-5">--}}
            {{--                    <h3>Activities</h3>--}}

            {{--                    <ul class="quick-links">--}}
            {{--                        <li>--}}
            {{--                            <a href="#">Maths Olympiad</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="#">Art Competition</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="#">English Novels</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="#">Science Competition</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="#">Teachers Day</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a href="#">World Kids Day</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Photo Gallery</h3>

                    <ul class="photo-gallery-list">
                        <li>
                            <div class="box">
                                <img src="{{asset('assets/nw3.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                        <li>
                            <div class="box">
                                <img src="{{asset('assets/ga2.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                        <li>
                            <div class="box">
                                <img src="{{asset('assets/ga3.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                        <li>
                            <div class="box">
                                <img src="{{asset('assets/ga4.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                        <li>
                            <div class="box">
                                <img src="{{asset('assets/ga5.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                        <li>
                            <div class="box">
                                <img src="{{asset('assets/ga6.jpg')}}" alt="image">
                                <a href="#" target="_blank" class="link-btn"></a>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Footer Area -->

<!-- Start Copy Right Area -->
<div class="copyright-area">
    <div class="container">
        <div class="copyright-area-content">
            <p>
                Copyright @<?php echo date("Y") ?> TMIBLOSSOM. All Rights Reserved
{{--                by--}}
{{--                <a href="https://envytheme.com/" target="_blank">--}}
{{--                    EnvyTheme--}}
{{--                </a>--}}
            </p>
        </div>
    </div>
</div>
<!-- End Copy Right Area -->
