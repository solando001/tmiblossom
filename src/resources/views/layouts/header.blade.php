<!-- Start Navbar Area -->
<div class="navbar-area">
    <div class="main-responsive-nav">
        <div class="container">
            <div class="main-responsive-menu">
                <div class="logo">
                    <a href="{{url('/')}}">
{{--                        <img src="{{asset('assets/img/logo.png')}}" alt="image">--}}
                        <img src="{{asset('assets/logo.png')}}" width="100px" alt="image">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="main-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{asset('assets/logo.png')}}" width="130px" alt="image">
                </a>
                @if(Auth::check())

                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="{{url('/home')}}" class="nav-link">
                                    Home
                                </a>
                            </li>

                            {{--                            <li class="nav-item">--}}
                            {{--                                <a href="{{url('add/staff')}}" class="nav-link">--}}
                            {{--                                    Add Staff--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}

                            <li class="nav-item">
                                <a href="{{url('add/event')}}" class="nav-link">
                                    Add Events
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{url('add/news')}}" class="nav-link">
                                    Add News
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>

                        </ul>
                    </div>

                @else
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link active">
                                    Home
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link active">
                                    About
                                    <i class='bx bx-chevron-down'></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item">
                                        <a href="{{url('about')}}" class="nav-link">
                                            About Us
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="{{url('proprietress')}}" class="nav-link">
                                            From The Proprietress Desk
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="{{url('curriculum')}}" class="nav-link">
                                            Our Curriculum
                                        </a>
                                    </li>

                                </ul>
                            </li>


                            <li class="nav-item">
                                <a href="{{url('contact')}}" class="nav-link">
                                    Contact
                                </a>
                            </li>




                            <li class="nav-item">
                                <a href="{{url('event')}}" class="nav-link">
                                    Event
                                </a>
                            </li>



                            <li class="nav-item">
                                <a href="{{url('news')}}" class="nav-link">
                                    News
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{url('gallery')}}" class="nav-link">
                                    Gallery
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{url('admission')}}" class="nav-link">
                                    Admission
                                </a>
                            </li>

{{--                            <li class="nav-item">--}}
{{--                                <a href="{{url('about')}}" class="nav-link">--}}
{{--                                    About--}}
{{--                                </a>--}}
{{--                            </li>--}}


                            <li class="nav-item">
                                <a href="https://tmiblossom.e-portal.com.ng/" target="_blank" class="nav-link">
                                    School Portal
                                </a>
                            </li>
                        </ul>

                    </div>
                @endif

            </nav>
        </div>
    </div>
</div>
<!-- End Navbar Area -->
