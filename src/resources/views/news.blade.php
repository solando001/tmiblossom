@extends('layouts.master')
@section('title', 'Covenant Child News')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>News</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>News</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Blog Area -->
    <section class="blog-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                @foreach($news as $new)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-item">
                        <div class="blog-image">
                            <a href="#">
                                <img src="{{$new->image}}" alt="image">
                            </a>
                        </div>

                        <div class="blog-content">
                            <ul class="post-meta">
                                <li>
                                    <span>By Admin:</span>
                                    <a href="#">{{$new->user->name}}</a>
                                </li>
                                <li>
                                    <span>Created:</span>
                                    {{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}
                                </li>
                            </ul>
                            <h3>
                                <a href="{{url('news/details/'.$new->slug)}}">{{$new->title}}</a>
                            </h3>
                            <p>
                                {!! substr(strip_tags($new->message) , 0, 200). '...' !!}
{{--                                {!! strip_tags($new->message) !!}--}}
                            </p>
{{--                            <div class="blog-btn">--}}
{{--                                <a href="#" class="default-btn">Read More</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-lg-12 col-md-12">
                    <div class="pagination-area">
                        <a href="#" class="prev page-numbers">
                            <i class='bx bx-chevron-left'></i>
                        </a>
                        <a href="#" class="page-numbers">1</a>
                        <span class="page-numbers current" aria-current="page">2</span>
                        <a href="#" class="page-numbers">3</a>
                        <a href="#" class="page-numbers">4</a>
                        <a href="#" class="next page-numbers">
                            <i class='bx bx-chevron-right'></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Area -->

@endsection
