@extends('layouts.master')
@section('title', 'From The Proprietress Desk')
@section('content')
    <div class="page-banner-area item-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>From The Proprietress Desk</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>The Proprietress Desk</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Privacy policy Area -->
    <section class="privacy-policy-area ptb-100">
        <div class="container">
            <div class="privacy-policy-accordion">
                <ul class="accordion">
                    <li class="accordion-item">
                        <a class="accordion-title active" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            From The Proprietress Desk
                        </a>

                        <p class="accordion-content show">
                            To God be the glory for His goodness and loving kindness towards us. We thank God for His
                            mercy and protection over the lives of the future leaders that are entrusted to our care.
                            On behalf of the chairman governing board, management board, staff and students of this great
                            school, I want to introduce our dear readers, friends and observers to our page.
                            <br>
                            I&#39;m glad to inform you that the Lord has been good to us as an institution, He has helped us to
                            achieve tremendous success academically, spiritually, morally and also to operate in excellence.

                            <br>
                            There is no doubt that academic performances is an area of immense interest to both parents
                            and school authority. We have been able to uphold and sustain our very high standard and help
                            the academically challenged students improve on their performance through various learning
                            support programmes.

                            <br>
                            Our vision of raising solution providers has made us committed to high academic standard. The
                            mental and intellectual development of the students is our utmost focus and parents will agree
                            with me that this is evident in the lives and performance of the students.<br>
                            Our language:<br>
                            You are great!
                            <br>
                            Discipline:<br>
                            As part of our duties to build up solution providers, the tool of discipline is very important. Thus
                            we enjoin all parents to co-operate with us in ensuring that sound discipline is inculcated in the
                            students. Let&#39;s start from home please!. Obey the school rules and regulations by complying
                            adequately.<br>
                            By the grace of God, I&#39;m proud to inform you that at TMI Blossom Academy, we raise future
                            leaders that will change the world positively, tomorrow spiritual prowess.
                            <br>
                            The school is highly committed to spiritual upbringing of the students. The students are taught
                            CRS where they learn spiritual moral teachings so as to differentiate between good and bad.
                            This is because our aim is to raise a positive child in a negative world. We also hold devotions
                            and fellowship at different times for their spiritual alertness. Worthy of mentioning is also the
                            fasting and prayer session we hold every term, which involves everybody for divine protection,
                            direction and to commit every of our activities into the hands of God. Kindly sustain the tempo
                            of their spiritual fervency at home and during the holiday by paying more attention to their
                            private activities like friends they keep, TV programmes and novel they read.
                            <br>

                            This write up will be incomplete withour reming our dear parents and prospective parents to
                            please compliment the learning process at TMI Blossom Academy by obeying the school rules
                            and regulations.<br>
                            - Let us lead by examples<br>
                            - Be positive role models<br>
                            - Let them know it&#39;s not ok to violate rules as we teach them at TMI Blossom Academy<br>
                            - Check your ward/child&#39;s report sheet and endorse appropriately<br>
                            - Censor the TV programmes the watch<br>
                            Be part of your child&#39;s up-bringing, don&#39;t leave it in the hand of care givers only. That could be
                            too dangerous.<br>
                            - Be spiritual. so much are happening in our world today, it is only spirituality that will help you
                            discern. Proverbs 22:6<br>
                            &#39;Direct you child

                            <br>
                            <b><i>Pastor(Mrs) Oluseyi Aboyeji</i></b>


                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Privacy policy Area -->


@endsection
