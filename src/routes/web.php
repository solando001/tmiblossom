<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});


Route::get('/', [App\Http\Controllers\GeneralController::class, 'index']);
Route::get('teaching/staff', [App\Http\Controllers\GeneralController::class, 'teaching_staff']);
Route::get('nonteaching/staff', [App\Http\Controllers\GeneralController::class, 'non_teaching_staff']);
Route::get('event', [App\Http\Controllers\GeneralController::class, 'event']);
Route::get('gallery', [App\Http\Controllers\GeneralController::class, 'gallery']);
Route::get('contact', [App\Http\Controllers\GeneralController::class, 'contact']);
Route::get('about', [App\Http\Controllers\GeneralController::class, 'about']);
Route::get('news', [App\Http\Controllers\GeneralController::class, 'news']);
Route::get('admission', [App\Http\Controllers\GeneralController::class, 'enroll']);
Route::post('enroll', [App\Http\Controllers\GeneralController::class, 'post_enroll']);

Route::get('news/details/{slug}', [App\Http\Controllers\GeneralController::class, 'news_details']);
Route::get('event/details/{slug}', [App\Http\Controllers\GeneralController::class, 'event_details']);
Route::get('curriculum', [App\Http\Controllers\GeneralController::class, 'curriculum']);
Route::get('proprietress', [App\Http\Controllers\GeneralController::class, 'proprietress']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('add/news', [App\Http\Controllers\HomeController::class, 'add_news']);
Route::get('add/event', [App\Http\Controllers\HomeController::class, 'add_event']);
Route::get('add/staff', [App\Http\Controllers\HomeController::class, 'add_staff']);
Route::post('post/news', [App\Http\Controllers\HomeController::class, 'post_news']);
Route::post('post/event', [App\Http\Controllers\HomeController::class, 'post_event']);
Route::post('post/staff', [App\Http\Controllers\HomeController::class, 'post_staff']);
